package com.online.market;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.arturogutierrez.Badges;
import com.github.arturogutierrez.BadgesNotSupportedException;

import io.paperdb.Paper;


public class SettingActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener
{
CheckBox notification;
TextView setting,settingLanguage;
Spinner spinner;
Button save;
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        notification=findViewById(R.id.notification_check_box);
        setting=findViewById(R.id.setting);
        settingLanguage=findViewById(R.id.setting_language);
        spinner=findViewById(R.id.spinner);

        save=findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingActivity.this,MainActivity.class);
                startActivity(i);
            }
        });
        notification.setChecked(true);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(notification.isChecked()){

                }
                else{
                    try {
                        Badges.removeBadge(SettingActivity.this);
                    } catch (BadgesNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        spinner.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(
                        this, R.array.labels_array,
                        // Layout for each item
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        Paper.init(getBaseContext());
        String language=Paper.book().read("language");
        if(Market.langCheck==1){
            spinner.setSelection(1);
        }
        else{
            spinner.setSelection(0);
        }
    }
    public  void updateView(String lang){
        Context context=LocaleHelper.setLocale(getApplicationContext(),lang);
        Resources resources=context.getResources();
         notification.setText(resources.getString(R.string.notification));;
         setting.setText(resources.getString(R.string.setting));
         settingLanguage.setText(resources.getString(R.string.language));

    }
    @SuppressLint("ResourceAsColor")
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        View v=spinner.getSelectedView();
        ((TextView)v).setTextColor(R.color.black);
        String spinner_item =adapterView.getItemAtPosition(position).toString();

        switch (spinner_item){
            case "English":

                Paper.book().write("language","en");
                updateView((String) Paper.book().read("language"));
                Market.langCheck=0;
                break;
            case "Amharic":

                Paper.book().write("language","am");
                updateView((String) Paper.book().read("language"));
                Market.langCheck=1;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
