package com.online.market;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.app.NotificationCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import io.paperdb.Paper;

//import io.paperdb.Paper;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener
{
   public Button loginBtn;
    public Button skipBtn;
    public EditText phoneTxt;
    public TextView loginLang;
    public static String key="0";
    Spinner spinner;
    ProgressBar progressLogin;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    Fragment fragment;

    NotificationCompat.Builder notification;
  //  FirebaseAuth mAuth;
    private static final int uniqueID = 45612;

//    @Override
//    protected  void attachBaseContext(Context newBase){
//        super.attachBaseContext(LocaleHelper.onAttach(newBase,"en"));
//    }
@Override
protected void attachBaseContext(Context newBase) {
    super.attachBaseContext(LocaleHelper.onAttach(newBase,"en"));
}
    @Override
    protected void onStart() {

        super.onStart();
//       mAuth = FirebaseAuth.getInstance();
//        if(mAuth.getCurrentUser()!=null){
//            Intent i = new Intent(LoginActivity.this,MainActivity.class);
//            startActivity(i);
//        }

    }
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MySingleton.getInstance(getApplicationContext());

        setContentView(R.layout.activity_login);
        loginBtn=findViewById(R.id.login_btn);
        loginLang=findViewById(R.id.login_lang);
        phoneTxt=findViewById(R.id.phone_login);
        progressLogin=findViewById(R.id.progress_login);

        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference("Users").child("user");
       spinner=findViewById(R.id.login_spinner);
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(
                        this, R.array.labels_array,
                        // Layout for each item
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        View v=spinner.getSelectedView();
        if(Market.langCheck==1){
            spinner.setSelection(1);
        }
        else{
            spinner.setSelection(0);
        }

        Paper.init(this);


        skipBtn=findViewById(R.id.skip_login);

        loginBtn.setOnClickListener(this);

        skipBtn.setOnClickListener(this);
        String localKey=readFromFile(getApplicationContext());
        if(!localKey.equalsIgnoreCase("")){
            key=localKey;
            Intent intent= new Intent(LoginActivity.this,MainActivity.class);
            startActivity(intent);
        }

    }
    public  void updateView(String lang){
        Context context=LocaleHelper.setLocale(getApplicationContext(),lang);
        Resources resources=context.getResources();
        loginLang.setText(resources.getString(R.string.language));
        loginBtn.setText(resources.getString(R.string.login));
        phoneTxt.setHint(resources.getString(R.string.phone_091215));
        skipBtn.setText(resources.getString(R.string.skip));
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }





    @Override
    public void onClick(View v) {
    switch (v.getId()){
        case R.id.login_btn:
            String number=phoneTxt.getText().toString().trim();
            if(number.isEmpty()){
                phoneTxt.setError("number is required");
                phoneTxt.requestFocus();
                return;

            }
            if(number.equalsIgnoreCase("11")){
                progressLogin.setVisibility(View.VISIBLE);
                progressLogin.setVisibility(View.INVISIBLE);
                Intent intent= new Intent(LoginActivity.this,Notification.class);
                startActivity(intent);
                return;
            }
            if(isNetworkAvailable()){
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressLogin.setVisibility(View.VISIBLE);
                        if (dataSnapshot.hasChild(number)){
                            key= number;
                            writeToFile(key,getApplicationContext());
                            progressLogin.setVisibility(View.INVISIBLE);
                            Intent intent= new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(intent);
                        }
                        else{
                            User user=new User(number);
                            key=number;
                            writeToFile(key,getApplicationContext());
                            databaseReference.child(number).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    progressLogin.setVisibility(View.INVISIBLE);
                                    Intent intent= new Intent(LoginActivity.this,MainActivity.class);
                                    startActivity(intent);
                                }
                            });
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            else{
                Toast.makeText(LoginActivity.this,"please check your connection",Toast.LENGTH_SHORT).show();
            }


            break;
        case R.id.skip_login:
            Intent y = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(y);
            break;

    }
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        View v=spinner.getSelectedView();
        ((TextView)v).setTextColor(R.color.black);
        String spinner_item =parent.getItemAtPosition(position).toString();
        switch (spinner_item){
            case "English":

                Paper.book().write("language","en");
                updateView((String) Paper.book().read("language"));
                Market.langCheck=0;
                break;
            case "Amharic":
                Paper.book().write("language","am");
                updateView((String) Paper.book().read("language"));
                Market.langCheck=1;
                break;
        }
    }

//    private void setLocale(String lang) {
//        Locale locale= new Locale(lang);
//        Locale.setDefault(locale);
//        Configuration configuration= new Configuration();
//        configuration.locale=locale;
//        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());
//        SharedPreferences.Editor editor = getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        editor.putString("My_Lang",lang);
//        editor.apply();
//
//    }
//    public  void loadLocal(){
//        SharedPreferences preferences= getSharedPreferences("Settings",MODE_PRIVATE);
//        String lang=preferences.getString("My_Lang","");
//        setLocale(lang);
//    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }
    public static void writeToFile(String data,Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("config.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
}
