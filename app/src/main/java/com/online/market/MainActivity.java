package com.online.market;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.NotificationCompat;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {
    Fragment fragment;
    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase,"en"));
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
             fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new Menu1();
                    break;
                case R.id.navigation_dashboard:
                    fragment = new Menu2();

                    break;
                case R.id.navigation_notifications:
                    fragment = new Menu3();
                    break;
                case R.id.navigation_most_view:
                    fragment = new Menu4();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
            return true;
        }
    };
public void updateNot(){
    notification = new NotificationCompat.Builder(getApplicationContext(),"Market");
    NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    nm.notify(uniqueID, notification.build());

    notification = new NotificationCompat.Builder(getApplicationContext(),"Market");
    notification.setAutoCancel(true);
    //notification.setLargeIcon(R.drawable.logo3);
    notification.setWhen(System.currentTimeMillis());
    notification.setContentTitle(getString(R.string.new_arrival));
    notification.setContentText(getString(R.string.there_are)+"   hello"+getString(R.string.new_item_added));
    Intent intent = new Intent(MainActivity.this, MainActivity.class);
    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    notification.setContentIntent(pendingIntent);
}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Menu1()).commit();
        if (Market.langCheck==1){
            LocaleHelper.setLocale(getBaseContext(),"am");
        }
        else {
            LocaleHelper.setLocale(getBaseContext(),"en");
        }




    }

    private void updateView(String language) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        MenuItem search = menu.findItem(R.id.search);
        androidx.appcompat.widget.SearchView searchView = (androidx.appcompat.widget.SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                fragment = new Menu5();
                DatabaseReference databaseReference=  FirebaseDatabase.getInstance().getReference("Items").child("Item");
                ((Menu5) fragment).mSearchQuery=databaseReference.orderByChild("title").startAt(s.toUpperCase()).endAt(s.toUpperCase()+"\uf8ff");
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    public static boolean isAppAvailable(Context context, String appName)
    {
        PackageManager pm = context.getPackageManager();
        try
        {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            return false;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu1:
                Intent i = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(i);
                break;
            case R.id.menu3:
                Intent intentA = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intentA);
                break;
            case R.id.menu2:
                LoginActivity.writeToFile("",getApplicationContext());
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
