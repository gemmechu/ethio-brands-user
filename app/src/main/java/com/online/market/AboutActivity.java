package com.online.market;

import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

import static com.online.market.MainActivity.isAppAvailable;

public class AboutActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);


        Element locationElement = new Element();
        locationElement.setTitle("Find Location");
        locationElement.setIconDrawable(R.drawable.about_icon_location);
        locationElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("geo:9.013379, 38.750668");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
        Element telegramElement = new Element();
        telegramElement.setTitle("telegram");
        telegramElement.setIconDrawable(R.drawable.about_icon_telegram);
        telegramElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appName = "org.telegram.messenger";
                final boolean isAppInstalled = isAppAvailable(getApplicationContext(), appName);
                if (isAppInstalled)
                {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/ethiobrands"));
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Telegram not Installed", Toast.LENGTH_SHORT).show();

                }
            }});
        Element phoneElement = new Element();
        phoneElement.setTitle("0920238243");
        phoneElement.setIconDrawable(R.drawable.about_icon_phone);
        phoneElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone="tel:0920238243";
                Uri uri = Uri.parse(phone);
                Intent it = new Intent(Intent.ACTION_DIAL, uri);
                startActivity(it);
            }
        });


        View aboutPage = new AboutPage(this)
                .isRTL(false)
                .setImage(R.drawable.logo4)

//                        .addItem(versionElement)
//                        .addItem(adsElement)
                .addGroup("Connect with us")
                .addItem(phoneElement)
                .addItem(telegramElement)
                .addFacebook("https://m.facebook.com/ethiocollection")
                .addItem(locationElement)
                .addWebsite("http://EthioBrands.com/")
                .create();

        setContentView(aboutPage);

    }




}
