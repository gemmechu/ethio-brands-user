package com.online.market;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ListViewHolder> implements Filterable {
    private List<Item> dataList;
    private Context context;
    private List<Item> dataListFiltered;
    private ListAdapterListener listener;
    public  static ImageButton likeBtn;

    DatabaseReference mdatabaseLike=FirebaseDatabase.getInstance().getReference("Items").child("Likes");
    DatabaseReference mdatabaseUsers=FirebaseDatabase.getInstance().getReference("Items").child("Users");
    DatabaseReference databaseReference=FirebaseDatabase.getInstance().getReference("Items").child("Item");
    String postRef;
    public ItemAdapter(List<Item> dataList) {
        this.dataList=dataList;

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFiltered = dataList;
                } else {
                    List<Item> filteredList = new ArrayList<>();
                    for (Item row : dataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getTitle().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    dataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFiltered;
                return filterResults;
            }
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFiltered = (ArrayList<Item>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ListViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        public TextView itemTitle,itemPrice,itemViews,itemLike,itemSize,itemMade,soldOut;
        public ImageView imageView;

        public FrameLayout phoneBtn;


        public ListViewHolder(final View view) {
            super(view);

//            firebaseDatabase= FirebaseDatabase.getInstance();
//            databaseReference=firebaseDatabase.getReference("Items");
            itemPrice=(TextView) itemView.findViewById(R.id.item_price_top);
            itemTitle=(TextView) itemView.findViewById(R.id.item_title);
            itemViews=(TextView) itemView.findViewById(R.id.item_view);
            itemLike=(TextView) itemView.findViewById(R.id.item_like);
            itemSize=(TextView) itemView.findViewById(R.id.item_size);
            itemMade=(TextView) itemView.findViewById(R.id.item_made);
            likeBtn=itemView.findViewById(R.id.like_btn);
            imageView= (ImageView)itemView.findViewById(R.id.item_image);
            soldOut=itemView.findViewById(R.id.sold_txt);
            phoneBtn=itemView.findViewById(R.id.phone_btn);
            view.setOnClickListener(this);
            likeBtn.setOnClickListener(this);
            imageView.setOnClickListener(this);
            phoneBtn.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            System.out.println("id:  "+v.getId());
            int pos= getAdapterPosition();
            Item item= dataList.get(pos);
            Intent i = new Intent(v.getContext(), DescriptionActivity.class);

            i.putExtra("itemId",item.getKey());
            // Toast.makeText(context,"hello",Toast.LENGTH_SHORT).show();
            v.getContext().startActivity(i);
            switch (v.getId()){

                case R.id.like_btn:
                    mdatabaseLike.child(item.getKey()).child(LoginActivity.key).removeValue();
                    mdatabaseUsers.child(LoginActivity.key).child(item.getKey()).removeValue();
                    Toast.makeText(context,"you unliked Item",Toast.LENGTH_SHORT).show();
                    DatabaseReference dbLikeCount=databaseReference.child(item.getKey()).child("like");
                    dbLikeCount.runTransaction(new Transaction.Handler() {
                        @NonNull
                        @Override
                        public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                            Integer currentValue=mutableData.getValue(Integer.class);
                            if(currentValue==null){
                                mutableData.setValue(1);
                            }
                            else{
                                mutableData.setValue(currentValue-1);
                            }

                            return Transaction.success(mutableData);
                        }

                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {

                        }
                    });

                    break;
                case  R.id.phone_btn:
                    String phone="tel:0920238243";
                    Uri uri = Uri.parse(phone);
                    Intent it = new Intent(Intent.ACTION_DIAL, uri);
                    context.startActivity(it);

                    break;


            }
        }
    }
    public ItemAdapter(Context context, List<Item> ourData, ListAdapterListener listAdapterListener) {
        this.dataList = ourData;
        this.dataListFiltered=ourData;
        this.context=context;
        this.listener=listAdapterListener;
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        this.context=itemView.getContext();
        return new ListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, final int i) {

        Item item = dataList.get(i);
        listViewHolder.itemPrice.setText(String.valueOf(item.getPrice()));
        listViewHolder.itemTitle.setText(item.getTitle());
        listViewHolder.itemSize.setText(item.getSize());
        listViewHolder.itemMade.setText(item.getMadeIn());

        listViewHolder.itemViews.setText(String.valueOf((item.getView())));
        listViewHolder.itemLike.setText(String.valueOf(item.getLike()));

        mdatabaseLike.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!LoginActivity.key.equalsIgnoreCase("0")){
                    if(dataSnapshot.child(item.getKey()).hasChild(LoginActivity.key)) {
                      likeBtn.setImageResource(R.drawable.ic_action_like_red);
                    }

                }
                else {
                     likeBtn.setImageResource(R.drawable.ic_action_likeb);


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        if(item.isSoldOut()){
            listViewHolder.soldOut.setVisibility(View.VISIBLE);
        }
        else{
            listViewHolder.soldOut.setVisibility(View.INVISIBLE);
        }

        Picasso.with(context).load(item.getPhotoUrl().get(0)).networkPolicy(NetworkPolicy.OFFLINE).fit().centerCrop().into(listViewHolder.imageView, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                Picasso.with(context).load(item.getPhotoUrl().get(0)).fit().centerCrop().into(listViewHolder.imageView);
            }
        });
//        postRef = databaseReference.getRef().getKey();




    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
    public interface ListAdapterListener {
        void onContactSelected(Item item);
    }

}

