package com.online.market;


public class About {
    private String location;
    private String phone;
    private String description;

    public About(String location, String phone, String description) {
        this.location = location;
        this.phone = phone;
        this.description = description;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }
}
