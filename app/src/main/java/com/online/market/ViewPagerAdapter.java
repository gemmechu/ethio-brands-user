package com.online.market;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    //private  Integer[] images={R.drawable.shoes,R.drawable.tshirt};
    private  ArrayList<String> images;

    public ViewPagerAdapter(Context context,ArrayList<String> images){
        this.context=context;
        this.images=images;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
    @Override
    public int getCount() {
       return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view==o;
    }
    @Override
    public Object instantiateItem(ViewGroup viewGroup, final int position){

        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.slider_layout,null);
        final ImageView imageView=view.findViewById(R.id.slider_image);
       // imageView.setImageResource(images[position]);
        Picasso.with(context).load(images.get(position)).networkPolicy(NetworkPolicy.OFFLINE).fit().centerCrop().into(imageView, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                Picasso.with(context).load(images.get(position)).fit().centerCrop().into(imageView);
            }
        });
        ViewPager vp= (ViewPager) viewGroup;
        vp.addView(view,0);
        return view;
    }
}
