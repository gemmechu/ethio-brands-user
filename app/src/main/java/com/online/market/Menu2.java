package com.online.market;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class Menu2 extends Fragment {

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference,mdatabaseUsers;
    private DatabaseReference mdatabaseLike;
    private RecyclerView recyclerView;
    static Context context;
    private boolean processLike=false;
    private DatabaseReference mdatabaseView;
    private boolean processView=false;
    ProgressBar progressBar;
    private Query mRecentQuery;
    FirebaseRecyclerAdapter firebaseRecyclerAdapter;
    Item item;
    public Menu2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_2, container, false);
        //   recyclerView.setAdapter(mAdapter);
        firebaseDatabase= FirebaseDatabase.getInstance();

        databaseReference=  firebaseDatabase.getReference("Items").child("Item");
        mdatabaseLike=firebaseDatabase.getReference("Items").child("Likes");
        mdatabaseView=firebaseDatabase.getReference("Items").child("Views");
        mdatabaseUsers = firebaseDatabase.getReference("Items").child("Users");
        context=getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.list_recycler_view_2);
        progressBar=view.findViewById(R.id.wait_progressbar_1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecentQuery=databaseReference.orderByKey();

        return view;
        // Inflate the layout for this fragment

    }


    @Override
    public void onStart() {
        super.onStart();
        firebaseRecyclerAdapter= new FirebaseRecyclerAdapter<Item, Menu1.ItemViewHolder>(
                Item.class,R.layout.list_item, Menu1.ItemViewHolder.class,mRecentQuery

        ) {
            @Override
            public Item getItem(int position) {
                return super.getItem(getItemCount() - 1 - position);
            }

            @Override
            protected void populateViewHolder(Menu1.ItemViewHolder viewHolder, Item model, final int position) {
                model =getItem(position);
                final String post_key= getRef(getItemCount() - 1 - position).getKey();
                viewHolder.setItemTitle(model.getTitle());
                viewHolder.setItemPrice(String.valueOf(model.getPrice()) );
                viewHolder.setItemLike(String.valueOf(model.getLike()) );
                viewHolder.setItemViews(String.valueOf(model.getView()) );
                viewHolder.setItemSize(model.getSize());
                viewHolder.setItemMade(model.getMadeIn());
                viewHolder.setSoldOut(model.isSoldOut());
                viewHolder.setImageView(model.getPhotoUrl().get(0));
               // viewHolder.setPhoneBtnTxt(String.valueOf(model.getPhone()));
                viewHolder.setLikeBtn(post_key);
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getChildrenCount()>0){
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                viewHolder.likeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!LoginActivity.key.equalsIgnoreCase("0")){
                            processLike=true;
                            if(processLike){
                                mdatabaseLike.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(processLike){
                                            if(dataSnapshot.child(post_key).hasChild(LoginActivity.key)){
                                                mdatabaseLike.child(post_key).child(LoginActivity.key).removeValue();
                                                mdatabaseUsers.child(LoginActivity.key).child(post_key).removeValue();
                                                Toast.makeText(context,"you unliked Item",Toast.LENGTH_SHORT).show();
                                                DatabaseReference dbLikeCount=databaseReference.child(post_key).child("like");
                                                dbLikeCount.runTransaction(new Transaction.Handler() {
                                                    @NonNull
                                                    @Override
                                                    public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                                                        Integer currentValue=mutableData.getValue(Integer.class);
                                                        if(currentValue==null){
                                                            mutableData.setValue(1);
                                                        }
                                                        else{
                                                            mutableData.setValue(currentValue-1);
                                                        }

                                                        return Transaction.success(mutableData);
                                                    }

                                                    @Override
                                                    public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {

                                                    }
                                                });
                                                processLike=false;
                                            }
                                            else{
                                                mdatabaseLike.child(post_key).child(LoginActivity.key).setValue(LoginActivity.key);
                                                mdatabaseUsers.child(LoginActivity.key).child(post_key).setValue(post_key);
                                                DatabaseReference dbLikeCount=databaseReference.child(post_key).child("like");
                                                dbLikeCount.runTransaction(new Transaction.Handler() {
                                                    @NonNull
                                                    @Override
                                                    public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                                                        Integer currentValue=mutableData.getValue(Integer.class);
                                                        if(currentValue==null){
                                                            mutableData.setValue(1);
                                                        }
                                                        else{
                                                            mutableData.setValue(currentValue+1);
                                                        }

                                                        return Transaction.success(mutableData);
                                                    }

                                                    @Override
                                                    public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {

                                                    }
                                                });
                                                processLike=false;
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                            }
                        }
                        else {
                            Toast.makeText(context,getString(R.string.please_login),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(v.getContext(), DescriptionActivity.class);
                        DatabaseReference dbViewCount=databaseReference.child(post_key).child("view");
                        if(!LoginActivity.key.equalsIgnoreCase("0")){
                            processView=true;
                            if(processView){
                                mdatabaseView.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(processView){
                                            if(dataSnapshot.child(post_key).hasChild(LoginActivity.key)){

                                            }
                                            else{
                                                mdatabaseView.child(post_key).child(LoginActivity.key).setValue(LoginActivity.key);
                                                DatabaseReference dbViewCount=databaseReference.child(post_key).child("view");
                                                dbViewCount.runTransaction(new Transaction.Handler() {
                                                    @NonNull
                                                    @Override
                                                    public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                                                        Integer currentValue=mutableData.getValue(Integer.class);
                                                        if(currentValue==null){

                                                        }
                                                        else{
                                                            mutableData.setValue(currentValue+1);
                                                        }

                                                        return Transaction.success(mutableData);
                                                    }

                                                    @Override
                                                    public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {

                                                    }
                                                });

                                                processView=false;
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                            }
                        }
                        i.putExtra("itemId", post_key);

                        startActivity(i);

                    }
                });

            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        View mView;
        public TextView itemTitle,itemPrice,itemViews,itemLike,itemSize,itemMade;
        public ImageView imageView;
        ImageButton likeBtn;
        DatabaseReference mdatabaseLike;
        FirebaseAuth mAuth;
        public FrameLayout phoneBtn;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            mView=itemView;
            mAuth = FirebaseAuth.getInstance();
            mdatabaseLike=FirebaseDatabase.getInstance().getReference("Items").child("Likes");
            mdatabaseLike.keepSynced(true);
            phoneBtn= itemView.findViewById(R.id.phone_btn);
            likeBtn=itemView.findViewById(R.id.like_btn);

            phoneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String phone="tel:0920238243";
                    Uri uri = Uri.parse(phone);
                    Intent it = new Intent(Intent.ACTION_DIAL, uri);
                    context.startActivity(it);
                }
            });
        }
        public  void setLikeBtn(final String postKey){
            mdatabaseLike.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.child(postKey).hasChild(LoginActivity.key)) {
                    if(dataSnapshot.child(postKey).hasChild(LoginActivity.key)){
                        likeBtn.setImageResource(R.drawable.ic_action_like_red);
                    }
                    }
                    else {
                        likeBtn.setImageResource(R.drawable.ic_action_likeb);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }


        public void setItemTitle(String title){
            itemTitle=(TextView) itemView.findViewById(R.id.item_title);
            itemTitle.setText(title);
        }
        public void setItemPrice(String title){
            itemPrice=(TextView) itemView.findViewById(R.id.item_price_top);
            TextView itemPrice2=(TextView) itemView.findViewById(R.id.item_price);
            itemPrice2.setText(title);
            itemPrice.setText(title);
        }
        public void setItemSize(String title){
            itemSize=(TextView) itemView.findViewById(R.id.item_size);
            itemSize.setText(title);
        }
        public void setItemMade(String title){
            itemMade=(TextView) itemView.findViewById(R.id.item_made);
            itemMade.setText(title);
        }
        public void setItemViews(String title){
            itemViews=(TextView) itemView.findViewById(R.id.item_view);
            itemViews.setText(title);
        }
        public void setItemLike(String title){
            itemLike=(TextView) itemView.findViewById(R.id.item_like);
            itemLike.setText(title);
        }
        public void setImageView(String url){
            imageView= (ImageView)itemView.findViewById(R.id.item_image);
            Picasso.with(context).load(url).into(imageView);
        }


    }

}
