package com.online.market;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import io.paperdb.Paper;

public class DescriptionActivity extends AppCompatActivity {
    TextView itemPriceDetail,itemTitleDetail,itemSizeDetail,itemMadeInDetail,itemLikeDetail,itemViewDetail,itemStatusDetail,detailName,detailPrice,detailMadeIn,detailSize,detailStatus,detailCall;
    FrameLayout itemPhoneDetail,itemLikeButton;
    ViewPager viewPager;
    private String itemKey=null;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReferenceAbout;
    private FirebaseAuth mAuth;
    LinearLayout slideDotPanel;
    private  ImageView[] dots;
    int dotCount;
    private ArrayList<String> images=new ArrayList<>();
    Item item;
    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        //itemImageDetail=findViewById(R.id.item_image_detail);
        firebaseDatabase= FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        databaseReference=firebaseDatabase.getReference("Items").child("Item");
        databaseReferenceAbout=firebaseDatabase.getReference("About");
        databaseReferenceAbout.keepSynced(true);
        viewPager=findViewById(R.id.view_pager);
        slideDotPanel=findViewById(R.id.slider_data);
        itemTitleDetail=findViewById(R.id.item_title_detail);
         itemPhoneDetail=findViewById(R.id.item_phone_detail);

        itemPriceDetail=findViewById(R.id.item_price_details);
        itemSizeDetail=findViewById(R.id.item_size_detail);
        itemMadeInDetail=findViewById(R.id.item_made_in_details);
        itemLikeDetail=findViewById(R.id.item_like_details);
        itemViewDetail=findViewById(R.id.item_view_details);
        itemStatusDetail=findViewById(R.id.item_status_details);

        detailName=findViewById(R.id.detail_name);
        detailMadeIn=findViewById(R.id.detail_made_in);
        detailPrice=findViewById(R.id.detail_price);
        detailSize=findViewById(R.id.detail_size);
        detailStatus=findViewById(R.id.detail_status);
        detailCall=findViewById(R.id.detail_call);



         itemKey= getIntent().getExtras().getString("itemId");


        databaseReference.child(itemKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 item= dataSnapshot.getValue(Item.class);
                 String title= (String) dataSnapshot.child("title").getValue();
                itemTitleDetail.setText(item.getTitle());
                images= item.getPhotoUrl();

                viewPagerAdapter = new ViewPagerAdapter(getApplicationContext(), images);
                viewPager.setAdapter(viewPagerAdapter);
                dotCount =viewPagerAdapter.getCount();
                dots=new ImageView[dotCount];
                for (int i=0; i<dotCount;i++){
                    dots[i]=new ImageView(getApplicationContext());
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_lens_non_active_24dp));
                    LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(8,0,8,0);
                    slideDotPanel.addView(dots[i],params);
                }
                dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_lens_active_24dp));
                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, float v, int i1) {
                        
                    }

                    @Override
                    public void onPageSelected(int i) {
                        for(int j=0;j<dotCount;j++){
                            dots[j].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_lens_non_active_24dp));
                        }
                        dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_lens_active_24dp));
                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {

                    }
                });
             //   itemPhoneDetail.setText("0"+String.valueOf(item.getPhone()));
                itemPhoneDetail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String phone="tel:0920238243";
                        Uri uri = Uri.parse(phone);
                        Intent it = new Intent(Intent.ACTION_DIAL, uri);
                        startActivity(it);
                    }
                });
                itemPriceDetail.setText(String.valueOf(item.getPrice()));
                itemSizeDetail.setText(item.getSize());
                itemMadeInDetail.setText(item.getMadeIn());
                itemLikeDetail.setText(String.valueOf(item.getLike()));
                itemViewDetail.setText(String.valueOf(item.getView()));
                if(item.isSoldOut()){
                    itemStatusDetail.setText(R.string.sold_out);
                }
            }



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Paper.init(getBaseContext());
        String language=Paper.book().read("language");
        if(Market.langCheck==1){
            updateView("am");
        }
        else
            updateView(("en"));
        }




    public  void updateView(String lang){
        Context context=LocaleHelper.setLocale(getApplicationContext(),lang);
        Resources resources=context.getResources();



        detailName.setText(resources.getString(R.string.name));
        detailMadeIn.setText(resources.getString(R.string.made_in));
        detailPrice.setText(resources.getString(R.string.price));
        detailSize.setText(resources.getString(R.string.size_detail));
        detailStatus.setText(resources.getString(R.string.status));
        detailCall.setText(resources.getString(R.string.call));



    }
}

