package com.online.market;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Menu3 extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth mAuth;
    private DatabaseReference databaseReference,databaseReferenceItem;

    List<Item> dataList=new ArrayList<>();
    List<Item> dataList1=new ArrayList<>();
    Iterable<DataSnapshot> iterable;
    List<String> strings=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_2, container, false);
        // use a linear layout manager
        recyclerView = view.findViewById(R.id.list_recycler_view_2);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        firebaseDatabase= FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        if(!LoginActivity.key.equalsIgnoreCase("0")){
            databaseReference=  firebaseDatabase.getReference("Items").child("Users").child(LoginActivity.key);
            databaseReferenceItem=firebaseDatabase.getReference("Items").child("Item");
            List<String> strings=new ArrayList<String>();
            // specify an adapter (see also next example)
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot keySnapShot: dataSnapshot.getChildren()) {
                        String key= (String) keySnapShot.getValue();
                        strings.add(key);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            databaseReferenceItem.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    for (DataSnapshot itemSnapShot: dataSnapshot.getChildren()) {

                        if(strings.contains(itemSnapShot.getKey()) ){

                            Item item = itemSnapShot.getValue(Item.class);
                            item.setKey(itemSnapShot.getKey());
                            Log.d("keys",item.getKey());
                            dataList.add(item);
                            mAdapter.notifyDataSetChanged();

                        }


                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        else{

        }



        mAdapter = new ItemAdapter(dataList);
        recyclerView.setAdapter(mAdapter);

        return view;
        // Inflate the layout for this fragment

    }


}

