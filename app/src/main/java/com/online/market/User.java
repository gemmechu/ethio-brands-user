package com.online.market;

public class User {
    private String key,phone;

    public User(String phone) {
        this.phone = phone;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getKey() {
        return key;
    }

    public String getPhone() {
        return phone;
    }
}
