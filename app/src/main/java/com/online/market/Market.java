package com.online.market;


import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

public class Market  extends Application  {

    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;
    public static int langCheck=0;
    final private String FCM_API = "https://fcm.googleapis.com/fcm/send";
    final private String serverKey = "key=" + "AAAAtlZLYBQ:APA91bE4x2xLR6nYW6tDASMH-oMOpr45X_Xa0zq5F4fVSne92l0Wi5mtjTDL2gduF5Q1y4NXCH6EMWvtw1r410t3-P9Q8oK8Z_LomQtuPJxjeo4280u8GJ0_WbeO9ZopCXHItgxfRcWF";

    final private String contentType = "application/json";

    final String TAG = "NOTIFICATION TAG";

    String NOTIFICATION_TITLE;
    String NOTIFICATION_MESSAGE;
    String TOPIC;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase,"en"));
    }



    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        Picasso.Builder builder =new Picasso.Builder(getApplicationContext());
        builder.downloader(new OkHttpDownloader(getApplicationContext(), Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);

    }
    public void newArrival(){

        DatabaseReference databaseReference=  FirebaseDatabase.getInstance().getReference("Items").child("Item");
        long current=System.currentTimeMillis();
        Log.d("date", String.valueOf(current));
        long startDay=1000;

        Query mQuery = databaseReference.orderByChild("date").startAt(startDay);


        //Builds notification and issues it
        notification = new NotificationCompat.Builder(getApplicationContext(),"Market");
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());
//        mQuery.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                int count= (int) dataSnapshot.getChildrenCount();
//                if (dataSnapshot.getChildrenCount()>0){
//                    notification = new NotificationCompat.Builder(getApplicationContext(),"Market");
//                    notification.setAutoCancel(true);
//                    //notification.setLargeIcon(R.drawable.logo3);
//                    notification.setWhen(System.currentTimeMillis());
//                    notification.setContentTitle(getString(R.string.new_arrival));
//                    notification.setContentText(getString(R.string.there_are)+" "+ count +" "+getString(R.string.new_item_added));
//                    Intent intent = new Intent(Market.this, MainActivity.class);
//                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//                    notification.setContentIntent(pendingIntent);
//
//                    //Builds notification and issues it
//                    NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//                    nm.notify(uniqueID, notification.build());
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        notification = new NotificationCompat.Builder(getApplicationContext(),"Market");
        notification.setAutoCancel(true);
        //notification.setLargeIcon(R.drawable.logo3);
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle(getString(R.string.new_arrival));
        notification.setContentText(getString(R.string.there_are)+"   hello"+getString(R.string.new_item_added));
        Intent intent = new Intent(Market.this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);
    }

}
