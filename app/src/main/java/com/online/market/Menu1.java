package com.online.market;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.github.arturogutierrez.Badges;
import com.github.arturogutierrez.BadgesNotSupportedException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class Menu1 extends Fragment {
    private List<Item> dataList = new ArrayList<>();
    //private ItemAdapter mAdapter;

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference,mdatabaseUsers,mdatabaseUsersData;
    private DatabaseReference mdatabaseLike;

    private RecyclerView recyclerView;
    static Context context;
    private boolean processLike=false;
    private DatabaseReference mdatabaseView;
    private boolean processView=false;

    FirebaseRecyclerAdapter firebaseRecyclerAdapter;
    Item item;
    static int likes;
    ProgressBar progressBar;
    Query mRecentQuery;
    List<String> strings=new ArrayList<String>();
    SearchView searchView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,@Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_menu_1, container, false);
        //   recyclerView.setAdapter(mAdapter);
        firebaseDatabase= FirebaseDatabase.getInstance();

         progressBar=view.findViewById(R.id.wait_progressbar);
        databaseReference=firebaseDatabase.getReference("Items").child("Item");

        mdatabaseLike=firebaseDatabase.getReference("Items").child("Likes");
        mdatabaseView=firebaseDatabase.getReference("Items").child("Views");
         mdatabaseUsers = firebaseDatabase.getReference("Items").child("Users");
        mdatabaseUsersData= firebaseDatabase.getReference("Users").child("user");
        mdatabaseUsers.keepSynced(true);
        mdatabaseView.keepSynced(true);

        databaseReference.keepSynced(true);
        mdatabaseLike.keepSynced(true);
        context=getContext();
         recyclerView = (RecyclerView) view.findViewById(R.id.list_recycler_view);
       // searchView=view.findViewById(R.id.search);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                setBadge((int) dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mRecentQuery=databaseReference.orderByKey();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.search:
                Toast.makeText(context,"hhh",Toast.LENGTH_SHORT).show();

                // Not implemented here
                return false;
            default:
                break;
        }

        return false;
    }
    public void setBadge(int count){
        try {
            Badges.setBadge(context, count);
        } catch (BadgesNotSupportedException badgesNotSupportedException) {

        }
    }
    @Override
    public void onStart() {
        super.onStart();


         firebaseRecyclerAdapter= new FirebaseRecyclerAdapter<Item, ItemViewHolder>(
                Item.class,R.layout.list_item,ItemViewHolder.class,mRecentQuery

        ) {
             @Override
             public Item getItem(int position) {
                 return super.getItem(getItemCount() - 1 - position);
             }


            @Override
            protected void populateViewHolder(final ItemViewHolder viewHolder, Item model, final int position) {
            model =getItem(position);
            final String post_key= getRef(getItemCount() - 1 - position).getKey();
            viewHolder.setItemTitle(model.getTitle());
            viewHolder.setItemPrice(String.valueOf(model.getPrice()) );

            viewHolder.setItemLike(String.valueOf(model.getLike()) );
            viewHolder.setItemViews(String.valueOf(model.getView()) );
            viewHolder.setItemSize(model.getSize());
            viewHolder.setItemMade(model.getMadeIn());

            viewHolder.setSoldOut(model.isSoldOut());
            viewHolder.setImageView(model.getPhotoUrl().get(0));
            //viewHolder.setPhoneBtnTxt(String.valueOf(model.getPhone()));
            viewHolder.setLikeBtn(post_key);


                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getChildrenCount()>0){
                            progressBar.setVisibility(View.GONE);
                        }
                        for (DataSnapshot Snapshot : dataSnapshot.getChildren()) {
                            if(Snapshot.getKey().toString().equalsIgnoreCase("photoUrl")){

                            }

                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(v.getContext(), DescriptionActivity.class);
                        DatabaseReference dbViewCount=databaseReference.child(post_key).child("view");

                        if(!LoginActivity.key.equalsIgnoreCase("0")){
                            processView=true;
                            if(processView){
                                mdatabaseView.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(processView){
                                            if(dataSnapshot.child(post_key).hasChild(LoginActivity.key)){

                                            }
                                            else{

                                                mdatabaseView.child(post_key).child(LoginActivity.key).setValue(LoginActivity.key);
                                                DatabaseReference dbViewCount=databaseReference.child(post_key).child("view");
                                                dbViewCount.runTransaction(new Transaction.Handler() {
                                                    @NonNull
                                                    @Override
                                                    public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                                                        Integer currentValue=mutableData.getValue(Integer.class);
                                                        if(currentValue==null){

                                                        }
                                                        else{
                                                            mutableData.setValue(currentValue+1);
                                                        }

                                                        return Transaction.success(mutableData);
                                                    }

                                                    @Override
                                                    public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {

                                                    }
                                                });

                                                processView=false;
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                            }
                        }
                        i.putExtra("itemId", post_key);

                        startActivity(i);

                    }
                });
                viewHolder.likeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!LoginActivity.key.equalsIgnoreCase("0")){
                            processLike=true;
                            if(processLike){
                                mdatabaseLike.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(processLike){
                                            if(dataSnapshot.child(post_key).hasChild(LoginActivity.key)){
                                                mdatabaseLike.child(post_key).child(LoginActivity.key).removeValue();
                                                mdatabaseUsers.child(LoginActivity.key).child(post_key).removeValue();
                                                Toast.makeText(context,"you unliked Item",Toast.LENGTH_SHORT).show();
                                                DatabaseReference dbLikeCount=databaseReference.child(post_key).child("like");
                                                dbLikeCount.runTransaction(new Transaction.Handler() {
                                                    @NonNull
                                                    @Override
                                                    public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                                                        Integer currentValue=mutableData.getValue(Integer.class);
                                                        if(currentValue==null){
                                                            mutableData.setValue(1);
                                                        }
                                                        else{
                                                            mutableData.setValue(currentValue-1);
                                                        }

                                                        return Transaction.success(mutableData);
                                                    }

                                                    @Override
                                                    public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {

                                                    }
                                                });
                                                processLike=false;
                                            }
                                            else{
                                                mdatabaseLike.child(post_key).child(LoginActivity.key).setValue(LoginActivity.key);
                                                mdatabaseUsers.child(LoginActivity.key).child(post_key).setValue(post_key);
                                                DatabaseReference dbLikeCount=databaseReference.child(post_key).child("like");
                                                dbLikeCount.runTransaction(new Transaction.Handler() {
                                                    @NonNull
                                                    @Override
                                                    public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                                                        Integer currentValue=mutableData.getValue(Integer.class);
                                                        if(currentValue==null){
                                                            mutableData.setValue(1);
                                                        }
                                                        else{
                                                            mutableData.setValue(currentValue+1);
                                                        }

                                                        return Transaction.success(mutableData);
                                                    }

                                                    @Override
                                                    public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {

                                                    }
                                                });
                                                processLike=false;
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                            }
                        }
                        else {
                            Toast.makeText(context,getString(R.string.please_login),Toast.LENGTH_SHORT).show();
                        }
                    }
                });


            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        View mView;
        public TextView itemTitle,itemPrice,itemViews,itemLike,itemSize,itemMade,soldOut;
        public ImageView imageView;
        public FrameLayout phoneBtn;
        ImageButton likeBtn;
        ProgressBar itemImageProgress;

        DatabaseReference mdatabaseLike;
        FirebaseAuth mAuth;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            mView=itemView;
            mAuth = FirebaseAuth.getInstance();
            mdatabaseLike=FirebaseDatabase.getInstance().getReference("Items").child("Likes");
            mdatabaseLike.keepSynced(true);
            phoneBtn= itemView.findViewById(R.id.phone_btn);

           likeBtn=itemView.findViewById(R.id.like_btn);

           phoneBtn.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   String phone="tel:0920238243";
                    Uri uri = Uri.parse(phone);
                    Intent it = new Intent(Intent.ACTION_DIAL, uri);
                    context.startActivity(it);
               }
           });
        }
        public  void setLikeBtn(final String postKey){
            final DatabaseReference db=FirebaseDatabase.getInstance().getReference("Items").child("Item");
            db.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    likes=dataSnapshot.child(postKey).child("like").getValue(Integer.class);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            mdatabaseLike.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(!LoginActivity.key.equalsIgnoreCase("0")){
                    if(dataSnapshot.child(postKey).hasChild(LoginActivity.key)) {
                        likeBtn.setImageResource(R.drawable.ic_action_like_red);
                    }

                 }
                    else {
                        likeBtn.setImageResource(R.drawable.ic_action_likeb);


                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

/*        public  void updateView(String lang){
            Context context=LocaleHelper.setLocale(getContext,lang);
            Resources resources=context.getResources();
        notification.setText(resources.getString(R.string.notification));;
        setting.setText(resources.getString(R.string.setting));
        settingLanguage.setText(resources.getString(R.string.language));

        }*/
        public void setItemTitle(String title){
            itemTitle=(TextView) itemView.findViewById(R.id.item_title);
            itemTitle.setText(title);
        }

        public void setSoldOut(boolean sold){
            soldOut=itemView.findViewById(R.id.sold_txt);
            if(sold){
                soldOut.setVisibility(View.VISIBLE);
            }
            else{
                soldOut.setVisibility(View.INVISIBLE);
            }
        }
        public void setItemPrice(String title){
            itemPrice=(TextView) itemView.findViewById(R.id.item_price_top);
            TextView itemPrice2=(TextView) itemView.findViewById(R.id.item_price);
            itemPrice2.setText(title);
            itemPrice.setText(title);
        }
        public void setItemSize(String title){
            itemSize=(TextView) itemView.findViewById(R.id.item_size);
            itemSize.setText(title);
        }
        public void setItemMade(String title){
            itemMade=(TextView) itemView.findViewById(R.id.item_made);
            itemMade.setText(title);
        }
        public void setItemViews(String title){
            itemViews=(TextView) itemView.findViewById(R.id.item_view);
            itemViews.setText(title);
        }
        public void setItemLike(String title){
            itemLike=(TextView) itemView.findViewById(R.id.item_like);
            itemLike.setText(title);
        }
        public void setImageView(final String url){
            imageView= (ImageView)itemView.findViewById(R.id.item_image);
            Picasso.with(context).load(url).networkPolicy(NetworkPolicy.OFFLINE).fit().centerCrop().into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(context).load(url).fit().centerCrop().into(imageView);
                }
            });
        }


    }
}
